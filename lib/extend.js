function extend(o,p) {
	if(!o) {
		throw new Error("parameter o is not passed");
	}

	if(!p) {
		throw new Error("p is not passed")
	}

	if(typeof o == "function" || typeof o == "object") {
		var args = arguments,
			refs = Array.prototype.slice.call(args, 1),
			obj;

		refs.forEach(function(v,i) {
			obj = refs[i];

			for(prop in obj) {
				o[prop] = obj[prop];
			}
		})
	} else{
		throw new TypeError("method is not an object nor a function")	
    }	
}

module.exports = extend;